import angular from 'angular';

// Create the module where our functionality can attach to
let incidencesModule = angular.module('app.incidences', []);

// Include our UI-Router config settings
import IncidencesConfig from './incidences.config';
incidencesModule.config(IncidencesConfig);


// Controllers
import IncidencesCtrl from './incidences.controller';
incidencesModule.controller('IncidencesCtrl', IncidencesCtrl);

import DetailsCtrl from './details.controller';
incidencesModule.controller('DetailsCtrl', DetailsCtrl);


export default incidencesModule;
