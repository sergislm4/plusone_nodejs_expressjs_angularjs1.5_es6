function IncidencesConfig($stateProvider) {
    'ngInject';

    $stateProvider
    .state('app.incidences', {
        url: '/incidences',
        controller: 'IncidencesCtrl',
        controllerAs: '$ctrl',
        templateUrl: 'incidences/incidences.html',
        title: 'Incidences'
    });
    $stateProvider
    .state('app.details', {
      url: '/incidences/:slug',
      controller: 'DetailsCtrl',
      controllerAs: '$ctrl',
      templateUrl: 'incidences/details.html',
      title: 'Details',
      resolve: {
        incidence: function(Incidences, $state, $stateParams) {
          return Incidences.get($stateParams.slug).then(
            (Incidences) => Incidences,
            (err) => $state.go('app.home')
          )
        }
      }
    });
    $stateProvider
    .state('app.confirm', {
      url: '/incidences/:slug/:token',
      controller: 'DetailsCtrl',
      controllerAs: '$ctrl',
      templateUrl: 'incidences/details.html',
      title: 'Details',
      resolve: {
        incidence: function(Incidences, $state, $stateParams) {
          return Incidences.get($stateParams.slug).then(
            (Incidences) => Incidences,
            (err) => $state.go('app.home')
          )
        },
        confirm: function(Incidences, $state, $stateParams) {
          return Incidences.confirm($stateParams.slug, $stateParams.token).then(
            (err) => $state.go('app.home')
          )
        }
      }
    }); 
  };
  
  export default IncidencesConfig;