class IncidencesCtrl {
    constructor(User, Incidences, AppConstants, $scope, $timeout, NgMap, $stateParams, $state, $filter, Tags) {
      'ngInject';

      this.appName = AppConstants.appName;
      this._$scope = $scope;
      this.Incidences = Incidences;
      
      var vm = this;
      // Get list of all tags
    Tags
    .getAll()
    .then(
      (tags) => {
        this.tagsLoaded = true;
        this.tags = tags
        console.log(this.tags);
      }
    );
      this.listConfig = {
        type: 'all'
      };
      
  
    }
  
    changeList(newList) {
      this._$scope.$broadcast('setListTo', newList);
    }
}

export default IncidencesCtrl;
