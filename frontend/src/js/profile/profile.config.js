function ProfileConfig($stateProvider) {
  'ngInject';

  $stateProvider
  .state('app.profile', {
    abstract: true,
    url: '/@:username',
    controller: 'ProfileCtrl',
    controllerAs: '$ctrl',
    templateUrl: 'profile/profile.html',
    resolve: {
      profile: function(Profile, $state, $stateParams) {
        return Profile.get($stateParams.username).then(
          (profile) => profile,
          (err) => $state.go('app.home')
        )
      }
    }

  })

  .state('app.profile.main', {
    url:'',
    controller: 'ProfileIncidencesCtrl',
    controllerAs: '$ctrl',
    templateUrl: 'profile/profile-incidences.html',
    title: 'Profile'
  })
  .state('app.profile.issued', {
    url:'/issued',
    controller: 'ProfileIncidencesCtrl',
    controllerAs: '$ctrl',
    templateUrl: 'profile/profile-incidences.html',
    title: 'Issued'
  });

};

export default ProfileConfig;
