export default class WebSocket {
    static get $inject() { return ['User']; }
    constructor(User,) {
      let host = 'http://localhost:8080';
      this.init(User, host);
    }
  
    init(User, host) {
      console.log("WEBSOCKET connecting to", host);

      this.socket = io.connect(host);
  
      this.socket.on('connect', () => {
        let sessionId = User.current.username;
  
        console.log("WEBSOCKET connected with session id", User.current.username);
  
        this.socket.emit('new_user', { id: sessionId });
      });
  
      this.socket.on('error', (error) => {
        console.log("WEBSOCKET - error", error)
      });
    }
  
    on(key, callback) {
      this.socket.on(key, (data) => {
        console.log("on", key, data)
        
          callback(data)
      });
    }

    new_message(data){
      this.socket.emit('new_message', data);
    }
  }


