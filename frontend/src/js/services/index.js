import angular from 'angular';

// Create the module where our functionality can attach to
let servicesModule = angular.module('app.services', []);


import UserService from './user.service';
servicesModule.service('User', UserService);

import JwtService from './jwt.service'
servicesModule.service('JWT', JwtService);

import ProfileService from './profile.service';
servicesModule.service('Profile', ProfileService);

import IncidencesService from './incidences.service';
servicesModule.service('Incidences', IncidencesService);

import TagsService from './tags.service';
servicesModule.service('Tags', TagsService);

import WebSocket from './web_socket.service';
servicesModule.service('WebSocket', WebSocket);

import ContactService from './contact.service';
servicesModule.service('Contact', ContactService);

import ToasterService from './toaster.service';
servicesModule.service('Toaster', ToasterService);


export default servicesModule;
