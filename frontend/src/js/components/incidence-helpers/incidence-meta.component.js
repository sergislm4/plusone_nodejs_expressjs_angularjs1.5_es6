let IncidenceMeta= {
  bindings: {
    incidence: '='
  },
  transclude: true,
  templateUrl: 'components/incidence-helpers/incidence-meta.html'
};

export default IncidenceMeta;
