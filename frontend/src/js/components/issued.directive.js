function issued() {
    'ngInject';
  
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        if (attrs.issued) {
            element.css({
                content: "",
                opacity: "0.7", 
                height: "100%",
                width: "100%",
                left: 0,
                top: 0,
          });
        }
      }
    };
  }
  
  export default issued;