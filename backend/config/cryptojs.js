var CryptoJS = require('crypto-js');
var key = 'maythe4';

exports.ecr = function(obj)
{
    return CryptoJS.AES.encrypt(JSON.stringify(obj), key);
};
exports.dcr = function(obj)
{
    return JSON.parse(CryptoJS.AES.decrypt(obj, key)
        .toString(CryptoJS.enc.Utf8));
};