// Imports: GraphQL
const { makeExecutableSchema } = require('apollo-server');
var mongoose = require('mongoose');
var Incidence = mongoose.model('Incidence');


const typeDefs = `
schema {
    query: Query
}
type Incidence {
    slug: String!
    name: String
    desc: String
    type: String
    client: String
    place: String
    latitude: String
    longitude: String
    date: String
    price: String
    tagList: [String]
    img: String
    issued: String
    issuedBy: String
}

type Query {
    incidences: [Incidence]!
    singleIncidence(slug: String!): Incidence!
}`;

const resolvers = {
    Query: {
        incidences: () => Incidence.find({}).exec(),
        singleIncidence: (slug) => Incidence.findOne({ slug: slug }).exec()
      }
}

const schema = makeExecutableSchema({ typeDefs, resolvers })

module.exports = {schema};