//An important improve if time is not an issue, could be using a more recent version of graphql
//Obviusly, using moduling concepts, this example is just a simple test, no poetry
const { schema } = require('./graphQLRouter.js');
// Imports: GraphQL
const { graphqlExpress, graphiqlExpress } = require('apollo-server-express');

//Using body parser to parse graphql data into json
var bodyParser = require('body-parser');

var router = require('express').Router(),
  swaggerUi = require('swagger-ui-express'),
  swaggerDocument = require('./swagger.json');

swaggerDocument.host="localhost:8080";
var handlingRouterSocket = require('./socket');


router.use('/', require('./users'));
router.use('/profiles', require('./profiles'));
router.use('/tags', require('./tags'));
router.use('/incidences', require('./incidences'));
router.use('/contact', require('./contact'));
router.use('/socket', handlingRouterSocket.router);
router.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
router.use('/graphql', bodyParser.json(), graphqlExpress({
  schema
}));
router.use('/graphiql', graphiqlExpress({endpoint: '/graphql',}));


router.use(function(err, req, res, next){
  if(err.name === 'ValidationError'){
    return res.status(422).json({
      errors: Object.keys(err.errors).reduce(function(errors, key){
        errors[key] = err.errors[key].message;

        return errors;
      }, {})
    });
  }

  return next(err);
});

module.exports = router;